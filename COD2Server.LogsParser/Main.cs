﻿using COD2Server.LogsParser.Entidades;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace COD2Server.LogsParser
{
    public partial class Main : Form
    {
        List<string[]> Estadisticas { get; set; }
        List<ServerLog> ServerLogs { get; set; }
        private string RutaArchivoLog { get; set; }
        private int IntervaloActualizacion { get; set; }
        private System.Windows.Forms.Timer vTimer; 
        
        public Main()
        {
            InitializeComponent();
            this.Text = "COD2 | Server Static Parser";
            this.txtAppMonitor.ScrollBars = ScrollBars.Vertical;
            this.txtAppMonitor.ReadOnly = true;
            this.PintarDivisor();
            this.LoguearActividad("Se inicio la app correctamente.");
            this.IntervaloActualizacion = int.Parse(ConfigurationManager.AppSettings["IntervaloActualizacionMinutos"].ToString());
            this.lblDescripcion.Text = "Cada " + this.IntervaloActualizacion + " minutos, se actualizarán los valores en la base de datos";
            this.ServerLogs = new List<ServerLog>();
            this.RutaArchivoLog = ConfigurationManager.AppSettings["RutaArchivoLog"].ToString();

            this.GuardarDatos(); //Se guardan los datos la primera vez que se ejecuta la aplicacion...
            this.IniciarLoopActualizacion();
        }

        public List<string[]> parseCSV(string path)
        {
            List<string[]> parsedData = new List<string[]>();

            try
            {
                using (var fs = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                using (var readFile = new StreamReader(fs, Encoding.Default))
                {
                    string line;
                    string[] row;

                    while ((line = readFile.ReadLine()) != null)
                    {
                        row = line.Split(';');
                        parsedData.Add(row);
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }

            return parsedData;
        }

        public void GenerarObjetosLogs(List<string[]> pListaKills)
        {
            this.ServerLogs.Clear();
            foreach (string[] pEstadistica in pListaKills)
            {
                if (pEstadistica.Length == 13)
                {
                    if (pEstadistica[0].Contains("K") || pEstadistica[0].Contains("D"))
                    {
                        ServerLog vServerLog = new ServerLog();
                        vServerLog.ServerTime = pEstadistica[0];
                        vServerLog.LogType = pEstadistica[0].Contains("K") ? LogType.Kill : LogType.Damage;
                        vServerLog.VictimaGUID = pEstadistica[1];
                        vServerLog.IdAtacado = int.Parse(pEstadistica[2]);
                        vServerLog.VictimaTeam = pEstadistica[3];
                        vServerLog.VictimaNombre = pEstadistica[4];
                        vServerLog.AtacanteGUID = pEstadistica[5];
                        vServerLog.IdAtacante = int.Parse(pEstadistica[6]);
                        vServerLog.AtacanteTeam = pEstadistica[7];
                        vServerLog.AtacanteNombre = pEstadistica[8];
                        vServerLog.AtacanteArma = pEstadistica[9];
                        vServerLog.DamageRealizado = Decimal.Parse(pEstadistica[10]);
                        vServerLog.DamageType = pEstadistica[11];
                        vServerLog.DamageLocation = pEstadistica[12];

                        this.ServerLogs.Add(vServerLog);
                    }
                }
            }
        }

        public void IniciarLoopActualizacion()
        {
            vTimer = new System.Windows.Forms.Timer();
            vTimer.Tick += new EventHandler(this.vTimer_Tick);
            vTimer.Interval = this.IntervaloActualizacion * 60 * 1000; // in miliseconds
            vTimer.Start();
        }

        private void vTimer_Tick(object sender, EventArgs e)
        {
            this.GuardarDatos();
        }

        public void GuardarDatos()
        {
            this.PintarDivisor();
            this.LoguearActividad("- Parseando Archivo...");
            this.Estadisticas = this.parseCSV(this.RutaArchivoLog);
            this.GenerarObjetosLogs(this.Estadisticas);
            this.LoguearActividad("- Archivo parseado correctamente...");
            if (this.DeboActualizarBase())
            {
                this.LoguearActividad("# --> Comienza la carga de datos a la base de datos.");

                string connString = ConfigurationManager.ConnectionStrings["default"].ConnectionString;
                MySqlConnection conn = new MySqlConnection(connString);
                conn.Open();
                MySqlCommand cmd = conn.CreateCommand();
                this.EliminarDatosActuales();

                this.LoguearActividad("- Cargando Registros nuevos desde archivo.");

                foreach (ServerLog vLog in this.ServerLogs)
                {
                    if (vLog.LogType == LogType.Kill)
                    {
                        cmd.CommandText = "INSERT INTO player_kills(PKiAtacanteNombre,PKiAtacanteEquipo,PKiAtacanteArma,PKiVictimaNombre,PKiVictimaEquipo, PKiDamageRealizado, PKiDamageLocation) ";
                        cmd.CommandText += "VALUES ('" + vLog.AtacanteNombre + "','" + vLog.AtacanteTeam + "','" + vLog.AtacanteArma + "','" + vLog.VictimaNombre + "', '" + vLog.VictimaTeam + "', '" + vLog.DamageRealizado.ToString() + "', '" + vLog.DamageLocation + "');";
                    }
                    else if (vLog.LogType == LogType.Damage)
                    {
                        cmd.CommandText = "INSERT INTO player_DamageDone(PDDAtacanteNombre,PDDAtacanteEquipo,PDDAtacanteArma,PDDVictimaNombre, PDDVictimaEquipo, PDDDamageRealizado, PDDDamageLocation) ";
                        cmd.CommandText += "VALUES ('" + vLog.AtacanteNombre + "','" + vLog.AtacanteTeam + "','" + vLog.AtacanteArma + "','" + vLog.VictimaNombre + "', '" + vLog.VictimaTeam + "', '" + vLog.DamageRealizado.ToString() + "', '" + vLog.DamageLocation + "');";
                    }
                    cmd.ExecuteNonQuery();
                }
                conn.Close();
                this.ActualizarContadorRegistros();
                this.LoguearActividad("# --> La carga de datos finalizó correctamente.");
            }
        }

        public void LoguearActividad(string pMensaje)
        {
            this.txtAppMonitor.Text += DateTime.Now + " :: " + pMensaje;
            this.txtAppMonitor.Text += "\r\n"; 
        }

        public void PintarDivisor()
        {
            this.txtAppMonitor.Text += " ============================================================== ";
            this.txtAppMonitor.Text += "\r\n";
        }

        public void EliminarDatosActuales()
        {
            this.LoguearActividad("- Eliminando los registros Actuales.");
            string connString = ConfigurationManager.ConnectionStrings["default"].ConnectionString;
            MySqlConnection conn = new MySqlConnection(connString);
            conn.Open();
            MySqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = "DELETE FROM player_Kills; DELETE FROM player_DamageDone";
            cmd.ExecuteNonQuery();
            conn.Close();
            this.LoguearActividad("- Datos eliminados correctamente...");
        }

        public void ActualizarContadorRegistros()
        {
            int vCantidad = this.ServerLogs.Count();
            string connString = ConfigurationManager.ConnectionStrings["default"].ConnectionString;
            MySqlConnection conn = new MySqlConnection(connString);
            conn.Open();
            MySqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = "UPDATE parametros SET ParCantidadRegistrosInsertados = " + vCantidad+ ";";
            cmd.ExecuteNonQuery();
            conn.Close();
            this.LoguearActividad("- Se actualizaron " + vCantidad + " registros...");
        }

        public bool DeboActualizarBase()
        {
            bool vRetorno = false;
            this.LoguearActividad("# --> Verificando si es necesario actualizar los datos...");

            string connString = ConfigurationManager.ConnectionStrings["default"].ConnectionString;
            MySqlConnection conn = new MySqlConnection(connString);
            conn.Open();
            MySqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = "SELECT ParCantidadRegistrosInsertados FROM parametros;";
            int vCantidadRegistros = (int)cmd.ExecuteScalar();
            conn.Close();

            if (vCantidadRegistros !=  this.ServerLogs.Count())
            {
                vRetorno = true;
            }
            else
            {
                this.LoguearActividad("- No es necesario, no se han registrado cambios...");
            }

            return vRetorno;
        }

    }
}
