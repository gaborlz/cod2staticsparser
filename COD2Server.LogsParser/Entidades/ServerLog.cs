﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COD2Server.LogsParser.Entidades
{
    public class ServerLog
    {
        public string ServerTime { get; set; }
        public LogType LogType { get; set; }
        public int IdAtacado {get; set;}
        public string VictimaGUID { get; set; }
        public string VictimaTeam { get; set; }
        public string VictimaNombre {get; set;}
        public int IdAtacante {get; set;}
        public string AtacanteGUID { get; set; }
        public string AtacanteTeam { get; set; }
        public string AtacanteNombre {get; set;}
        public string AtacanteArma { get; set; }
        public decimal DamageRealizado { get; set; }
        public string DamageType { get; set; }
        public string DamageLocation { get; set; }
    }

    public enum LogType
    {
        Kill,
        Damage,
        RoundWin,
        RoundLoss,
        PlayerJoin,
        PlayerQuit,
        Chat,
    }
}
